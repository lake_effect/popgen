#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate rand;
extern crate postgres;
extern crate names;
extern crate time;
extern crate rocket;

use std::thread;
use std::str;

use rand::{ThreadRng, seq, thread_rng};
use rand::distributions::{Exp, IndependentSample, Range};

use postgres::{Connection, TlsMode};

use names::{Generator, Name};

use time::PreciseTime;

use rocket::response::status::BadRequest;

/// Database URL
static DB_ADDR: &str = "postgres://postgres@localhost:5432";
/// Number of cores to use for record insert
static CORE_COUNT: i32 = 4;
/// Assuming there are 1000 locations around which points are clustered
static CITY_COUNT: usize = 1000;
/// Assuming there are 10M records
static REC_COUNT: usize = 10000000;
/// Assuming laplace scale of 0.1 is ok for generating places of interest
static SCALE: f64 = 0.1;
/// Count of results to return from internal benchmarks
static QUERY_COUNT: i64 = 30;
/// Radius at 300km for internal benchmarks
static RADIUS: f64 = 300000.0;

#[derive(Debug)]
struct Location {
    id: Option<i32>,
    name: String,
    lat: f64,
    lng: f64
}

fn main() {
    make_table()                                     // Initialize DB
        .and_then(|conn| { prep_data(conn, false) }) // Create random rows and locations to search on
        .and_then(|(conn, means)| {                  // Benchmark 10M nearby items and 10M closest items
            time_queries(conn, means)
        })
//        .and_then(cleanup_db)                      // Cleanup DB
        .unwrap();

    rocket::ignite().mount("/", routes![get_nearby, get_closest]).launch();
}

#[get("/nearby/<lng>/<lat>/<radius>/<limit>")]
fn get_nearby(lng: f64, lat: f64, radius: f64, limit: i64) -> Result<String, BadRequest<()>> {
    Connection::connect(DB_ADDR, TlsMode::None)
        .and_then(|conn| {
            nearby(&conn, lat, lng, radius, limit)
        })
        .or(Err(BadRequest::<()>(None)))
}

#[get("/closest/<lng>/<lat>/<limit>")]
fn get_closest(lng: f64, lat: f64, limit: i64) -> Result<String, BadRequest<()>> {
    Connection::connect(DB_ADDR, TlsMode::None)
        .and_then(|conn| {
            closest(&conn, lat, lng, limit)
        })
        .or(Err(BadRequest::<()>(None)))
}


/// Generates a lat/lng that we will use as the mean of other samples
fn gen_city(mut rng: &mut ThreadRng) -> [f64; 2] {
    // We assume that the cities are concentrated away from the poles for
    // latitude, but otherwise roughly uniformly distributed.
    let lat_range = Range::new(-50_f64, 50_f64);
    let lng_range = Range::new(-180_f64, 180_f64);

    [lat_range.ind_sample(&mut rng), lng_range.ind_sample(&mut rng)]
}

/// Generates a Location near a random one of the means supplied.
fn gen_loc(means: &Vec<[f64; 2]>, gen: &mut Generator) -> Location {
    let mut thread_rng = thread_rng();
    let center = seq::sample_iter(&mut thread_rng, means, 1).unwrap()[0];

    let point = [gen_laplace(&mut thread_rng, SCALE) + center[0],
                 gen_laplace(&mut thread_rng, SCALE) + center[1]];

    Location {
        id: None,
        name: gen.next().unwrap(),
        lat: point[0],
        lng: point[1]
    }
}

/// Draws from a Laplace variable.
fn gen_laplace(rng: &mut ThreadRng, scale: f64) -> f64 {
    let exp = Exp::new(1.0 / scale);
    exp.ind_sample(rng) - exp.ind_sample(rng)
}

fn insert_rows(conn: Connection, rows: Vec<Location>) -> postgres::Result<Connection> {
    {
        let stmt = conn.prepare("INSERT INTO loc_small (name, lng, lat) VALUES ($1, $2, $3)").unwrap();
        for loc in rows {
            stmt.execute(&[&loc.name.as_str(), &loc.lat, &loc.lng])?;
        }
    }
    Ok(conn)
}

fn make_table() -> postgres::Result<Connection> {
    Connection::connect(DB_ADDR, TlsMode::None)
        .and_then(|conn| {
            match conn.execute("CREATE TABLE loc_small (
                           id       SERIAL PRIMARY KEY,
                           name     VARCHAR NOT NULL,
                           lng      DOUBLE PRECISION NOT NULL,
                           lat      DOUBLE PRECISION NOT NULL
                         )", &[]) {
                Ok(_) => {
                    println!("Created table ok");
                    make_gist_index(conn)
                },
                Err(err) => {
                    println!("Didn't create table: {}", err);
                    Ok(conn)
                }
            }
        })
}

fn make_gist_index(conn: Connection) -> postgres::Result<Connection> {
    conn.execute("CREATE INDEX ON loc_small USING gist(ll_to_earth(lat, lng))", &[]).and(Ok(conn))
}

fn drop_table(conn: Connection) -> postgres::Result<Connection> {
    conn.execute("DROP TABLE loc_small", &[]).and(Ok(conn))
}

fn cleanup_db(conn: Connection) -> postgres::Result<()> {
    println!("Removing table...");
    drop_table(conn)
        .and_then(|conn| conn.finish() )
}

fn nearby(conn: &Connection, lat: f64, lng: f64, radius: f64, limit: i64) -> postgres::Result<String> {
    match conn.query("
      SELECT array_to_json(array_agg(t)) FROM (
        SELECT l.name, l.lat, l.lng FROM loc_small l
        WHERE earth_box(ll_to_earth($1, $2), $3) @> ll_to_earth(l.lat, l.lng)
        AND earth_distance(ll_to_earth($1, $2), ll_to_earth(l.lat, l.lng)) < $3
        ORDER BY 1 LIMIT $4
      ) t;
    ", &[&lat, &lng, &radius, &limit]) {
      Ok(rows) => {
          Ok(str::from_utf8(rows.get(0).get_bytes(0).unwrap_or(&[])).unwrap().to_string())
      },
      Err(err) => {
          println!("Nearby query failed: {}", err);
          Err(err)
      }
    }
}

fn closest(conn: &Connection, lat: f64, lng: f64, limit: i64) -> postgres::Result<String> {
    match conn.query("
      SELECT array_to_json(array_agg(t)) FROM (
        SELECT l.name, l.lat, l.lng FROM loc_small l
        WHERE earth_box(ll_to_earth($1, $2), 10000000) @> ll_to_earth(l.lat, l.lng) LIMIT $3
      ) t;
    ", &[&lat, &lng, &limit]) {
      Ok(rows) => {
          Ok(str::from_utf8(rows.get(0).get_bytes(0).unwrap_or(&[])).unwrap().to_string())
      },
      Err(err) => {
          println!("Closest query failed: {}", err);
          Err(err)
      }
    }
}

fn time_queries(conn: Connection, means: Vec<[f64;2]>) -> postgres::Result<Connection> {
    let mut thread_rng = thread_rng();
    {
        let mut times = vec![];

        for _ in 0..10 {
            let city = seq::sample_iter(&mut thread_rng, &means, 1).unwrap()[0];
            let start = PreciseTime::now();
            nearby(&conn, city[0], city[1], RADIUS, QUERY_COUNT)
                .and_then(|rows| {
                    let end = PreciseTime::now();
                    let time = start.to(end);
                    times.push(time.num_milliseconds());
                    println!("Nearby query took {}.", time);
                    Ok(rows)
                }).unwrap();
        }

        println!("Average query time is {} milliseconds.\n",
                 times.iter().fold(0, |sum, val| sum + val) as f64 / times.len() as f64);
    }
    {
        let mut times = vec![];

        for _ in 0..10 {
            let city = seq::sample_iter(&mut thread_rng, &means, 1).unwrap()[0];
            let start = PreciseTime::now();
            closest(&conn, city[0], city[1], QUERY_COUNT)
                .and_then(|rows| {
                    let end = PreciseTime::now();
                    let time = start.to(end);
                    times.push(time.num_milliseconds());
                    println!("Closest query took {}.", time);
                    Ok(rows)
                }).unwrap();
        }
        println!("Average query time is {} milliseconds.",
                 times.iter().fold(0, |sum, val| sum + val) as f64 / times.len() as f64);
    }

    Ok(conn)
}

fn prep_data(conn: Connection, insert: bool) -> postgres::Result<(Connection, Vec<[f64; 2]>)> {
    let mut handles = vec![];
    let mut rng = thread_rng();

    // First, generate 1000 means in lat/lng
    let mut means = Vec::with_capacity(CITY_COUNT);

    for _ in 1..CITY_COUNT {
        means.push(gen_city(&mut rng));
    }

    // Are we inserting records into the DB?
    if insert {
        println!("Entering generation cycle...");

        let thread_rec_count = (REC_COUNT as f64 / CORE_COUNT as f64) as usize;

        for thread_ind in 1..(CORE_COUNT + 1) {
            let thread_means = means.clone();

            handles.push(thread::spawn(move || {
                Connection::connect(DB_ADDR, TlsMode::None)
                    .and_then(|conn| {
                        let mut name_gen = Generator::with_naming(Name::Numbered);
                        let mut locs = Vec::with_capacity(thread_rec_count);
                        for _ in 1..thread_rec_count {
                            locs.push(gen_loc(&thread_means, &mut name_gen));
                        }

                        insert_rows(conn, locs)
                    })
                    .and_then(|conn| {
                        println!("thread {} complete.", thread_ind);
                        conn.finish()
                    })
                    .unwrap()
            }));
        }

        for handle in handles { handle.join().unwrap(); }
    }

    Ok((conn, means))
}
